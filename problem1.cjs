function Problem1(inventory, carId) {
    let arr = []
    if (inventory == undefined || inventory.length == 0 || inventory.constructor != Array) {
        return arr
    } else {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].id == carId) {
                return inventory[index]
            }
        }
        return arr
    }

}
module.exports = Problem1
// console.log()















// In case of Array.isArray(inventory) if we will pass {} it will send false value

// In case of [{}], inventory != array it will send true so our code will not work

// In case of [],{},[{}] inventory.constructor will send only the type of function not true or false