function problem4(inventory) {
    let arr = []
    if (inventory == undefined || inventory.length == 0 || inventory.constructor != Array) {
        return arr
    } else {
        for (let index = 0; index < inventory.length; index++) {
            arr.push(inventory[index].car_year)
        }
        return arr
    }
}
module.exports = problem4