function problem5(inventory, carYear) {
    let count = 0;
    let arr = []
    if (inventory == undefined || inventory.length == 0 || inventory.constructor != Array) {
        return arr
    } else {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_year < carYear) {
                count++
                arr.push(inventory[index])
            }
        }
        return arr

    }
}
module.exports = problem5