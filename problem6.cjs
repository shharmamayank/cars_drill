function problem6(inventory, carName1, carName2) {
    let arr = []
    if (inventory == undefined || inventory.length == 0 || inventory.constructor != Array) {
        return arr
    } else {

        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_make == carName1 || inventory[index].car_make == carName2) {
                arr.push(inventory[index])
            }

        }
        return JSON.stringify(arr)
    }
}
module.exports = problem6