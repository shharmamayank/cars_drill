function problem3(inventory) {
    let arr = []
    if (inventory == undefined || inventory.length == 0 || inventory.constructor != Array) {
        return arr
    } else {
        for (let index = 0; index < inventory.length; index++) {
            let Model = inventory[index].car_model;
            let upperCase = Model.toUpperCase()

            arr.push(upperCase)
        }
        arr.sort()
        return arr

    }

}
module.exports = problem3